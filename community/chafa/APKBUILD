# Contributor: knuxify <knuxify@gmail.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: knuxify <knuxify@gmail.com>
pkgname=chafa
pkgver=1.12.2
pkgrel=0
pkgdesc="Terminal graphics for the 21st century"
url="https://hpjansson.org/chafa"
arch="all"
license="LGPL-3.0-or-later"
makedepends="glib-dev freetype-dev libjpeg-turbo-dev tiff-dev libwebp-dev"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
source="https://github.com/hpjansson/chafa/releases/download/$pkgver/chafa-$pkgver.tar.xz"

# librsvg depends on rust, which is not available on that arches currently
case "$CARCH" in
	riscv64|s390x) ;;
	*) makedepends="$makedepends librsvg-dev";;
esac

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-gtk-doc \
		--without-imagemagick
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
88a356b33d6ecd4e2049fe5a8704a058bb651d47a22d538338be98680722a7df5a41a0439ef9ee350a03fd1764637d6d539df75f0f8a3f7555b3c25712090fe0  chafa-1.12.2.tar.xz
"
