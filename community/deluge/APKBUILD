# Contributor: August Klein <amatcoder@gmail.com>
# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=deluge
pkgver=2.1.0
pkgrel=0
pkgdesc="lightweight, Free Software, cross-platform BitTorrent client"
url="https://deluge-torrent.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	gtk+3.0
	py3-cairo
	py3-chardet
	py3-distro
	py3-gobject3
	py3-libtorrent-rasterbar
	py3-mako
	py3-openssl
	py3-pillow
	py3-rencode
	py3-service_identity
	py3-setproctitle
	py3-setuptools
	py3-six
	py3-twisted
	py3-wheel
	py3-xdg
	py3-zope-interface
	"
makedepends="
	py3-build
	py3-installer
	py3-wheel
	"
checkdepends="
	py3-mock
	py3-pytest
	"
options="!check" # failing ui off-by-one size tests, segfault in another
subpackages="$pkgname-doc"
source="http://download.deluge-torrent.org/source/${pkgver%.*}/deluge-$pkgver.tar.xz"

replaces="$pkgname-lang" # Overwrite removed subpackage

build() {
	python3 -m build --no-isolation --wheel
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/deluge-$pkgver-py3-none-any.whl
}

sha512sums="
4bfd4cc67b1a65dd0388e77d04db5b4d131471847a81e526f623328de68fa2a8b8953aeeaad10a13f39ec4960c91248d5af633c16202cba00c70f927d06e82c3  deluge-2.1.0.tar.xz
"
